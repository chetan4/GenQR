CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider               => $PROVIDER,  
    :aws_access_key_id      => $ACCESS_KEY_ID,    
    :aws_secret_access_key  => $SECRET_ACCESS_KEY
  }
  config.fog_directory  =  $BUCKET
  config.fog_public = false                             
end

