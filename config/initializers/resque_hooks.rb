require 'resque/scheduler'
Resque.schedule = YAML.load_file(File.join(Rails.root,'config','qrcode_scheduler.yml'))
Resque.before_fork do 
  ActiveRecord::Base.verify_active_connections!
end
## THIS CAN BE REMOVED NEED TO CROSS CHECK THOUGH
## If Redis Connection go away connect here
Resque.after_fork do 
  ActiveRecord::Base.establish_connection($CONFIG)
end
