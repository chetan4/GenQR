module CarrierWave
  module CSVProcessor
    def preprocess_csv
      row_no = 0
      product_attributes = []
      product_value = [] 
      product_hash  = []

      CSV.foreach(File.join(Rails.root,'public',self.to_s)) do |row|
        row_no +=1
        row_records = row.collect { |record| record.strip unless record.nil? }
        if (row.empty? || row_records.length == 1 && row_records[0].empty?)
          next  
        end
        
        if product_attributes.empty?
          product_attributes = row.collect(&:strip)
          next
        end
        record_hash = Hash.new
        
        row.each_with_index do |record,index|
          record_hash.merge(product_attributes[index].to_sym => record)  
        end
        
        product = Product.new(record_hash)
        if product.valid?
           product_hash << row
        else
          ValidationError.create(:row_id => row_no ,:error_encountered => product.errors.to_yaml)
        end 
        row_records,product,record_hash=[nil,nil,nil]
      end
      unless product_hash.empty?
        Product.import(product_attributes,product_hash)
      end
    end 
  end
end
