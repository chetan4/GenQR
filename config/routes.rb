GenQR::Application.routes.draw do 

  
  
  resources :assets  do 
    member do
      get 'remove_template'
      get 'all_sticker'
      get 'download_sticker'
      get 'validation_error'
      get 'print_sticker'
    end
    collection do 
      get 'failed_asset'
    end
  end
  
  resources :sticker_templates do
    member do 
      get 'sample_template'
      put 'update_copy'
    end  
  end

  mount Resque::Server.new, :at => "/resque"

  #root :to => "devise/sessions#new"
  
  
  match '/welcome' => "home#index",:as => :home
  match '/thank_you' => "home#thank_you",:as => :thank_you  


  devise_for :users

  devise_scope :user do
    get "/" => "devise/sessions#new",:as => :login 
    get "/logout" => "devise/sessions#destroy",:as => :logout
  end

  resources :users 
  

  
  resources :products do 
    member do 
      get 'qrcode_image'
      get 'regenerate_qrcode'
    end
  end
  
  resources :stickers do
    collection do 
      get 'create_sticker' 
    end
  end
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
