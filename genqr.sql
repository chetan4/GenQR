-- MySQL dump 10.13  Distrib 5.1.41, for debian-linux-gnu (i486)
--
-- Host: localhost    Database: GenQR_development
-- ------------------------------------------------------
-- Server version	5.1.41-3ubuntu12.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_encountered` text COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uploaded_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (2,'asset_sample.csv','asset_sample.csv',NULL,'successful','2012-04-06 16:36:46','2012-04-06 16:36:46','2012-04-06 16:36:46');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `make` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `qrcode_url` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (9,'FREIGHTLINER','CL120','6PV06567',2521,NULL,'Dallas','2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://FREIGHTLINERCL1206PV06567.com'),(10,NULL,NULL,NULL,2520,'1FUJA6CK06LV06526','Austin','2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://1FUJA6CK06LV06526.com'),(11,'VOLVO','VNM64T','8N491868',2664,NULL,'Dallas','2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://VOLVOVNM64T8N491868.com'),(12,NULL,NULL,NULL,2665,'4V4MC9EJ18N491874','Austin','2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://4V4MC9EJ18N491874.com'),(13,NULL,NULL,NULL,2712,'1XPWD40X68D768572','CUMMINS ISX600','2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://1XPWD40X68D768572.com'),(14,'PETERBILT','PETE388','768573',2713,NULL,'CUMMINS ISX600','2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://PETERBILTPETE388768573.com'),(15,'FREIGHTLINER','CL120',NULL,NULL,'1FUJA6CK9ADAV5490',NULL,'2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://FREIGHTLINERCL1201FUJA6CK9ADAV5490.com'),(16,'VOLVO','VNM64T',NULL,NULL,'4V4MC9EH1CN537483',NULL,'2012-04-06 16:36:46','2012-04-06 16:36:53',2,'http://VOLVOVNM64T4V4MC9EH1CN537483.com');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20120328124409'),('20120328124555'),('20120328133245'),('20120329115827'),('20120329141227'),('20120329142625'),('20120330090041'),('20120330090120'),('20120401173713'),('20120402075723'),('20120405104450');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sessions_on_session_id` (`session_id`),
  KEY `index_sessions_on_updated_at` (`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,'f95c95d7f0493aa4a7a7b478e6a91a9f','BAh7B0kiE3VzZXJfcmV0dXJuX3RvBjoGRUYiBi9JIgpmbGFzaAY7AEZvOiVB\nY3Rpb25EaXNwYXRjaDo6Rmxhc2g6OkZsYXNoSGFzaAk6CkB1c2VkbzoIU2V0\nBjoKQGhhc2h7ADoMQGNsb3NlZEY6DUBmbGFzaGVzewY6CmFsZXJ0SSI2WW91\nIG5lZWQgdG8gc2lnbiBpbiBvciBzaWduIHVwIGJlZm9yZSBjb250aW51aW5n\nLgY7AFQ6CUBub3cw\n','2012-04-06 13:28:47','2012-04-06 13:28:47'),(3,'a764831798996f629afd27a8517891b7','BAh7CEkiEF9jc3JmX3Rva2VuBjoGRUZJIjEvcXU1VGFVUFcwUzczUVVFM3Nn\ncE93OHRkQ2RLTXcza2xuWkdKbG5KRnlBPQY7AEZJIgpmbGFzaAY7AEZvOiVB\nY3Rpb25EaXNwYXRjaDo6Rmxhc2g6OkZsYXNoSGFzaAk6CkB1c2VkbzoIU2V0\nBjoKQGhhc2h7BjoLbm90aWNlVDoMQGNsb3NlZEY6DUBmbGFzaGVzewY7Ckki\nHFNpZ25lZCBpbiBzdWNjZXNzZnVsbHkuBjsAVDoJQG5vd286JEFjdGlvbkRp\nc3BhdGNoOjpGbGFzaDo6Rmxhc2hOb3cGOgtAZmxhc2hACUkiGXdhcmRlbi51\nc2VyLnVzZXIua2V5BjsAVFsISSIJVXNlcgY7AEZbBmkGSSIiJDJhJDEwJFF4\ncFpMN0VDalVVSFZXeFRhRzVQdHUGOwBU\n','2012-04-06 14:35:07','2012-04-06 14:35:08'),(6,'3fb5fa0341064568357044b67c05bbfe','BAh7CEkiCmZsYXNoBjoGRUZvOiVBY3Rpb25EaXNwYXRjaDo6Rmxhc2g6OkZs\nYXNoSGFzaAk6CkB1c2VkbzoIU2V0BjoKQGhhc2h7BjoKYWxlcnRUOgxAY2xv\nc2VkRjoNQGZsYXNoZXN7BjsKSSI2WW91IG5lZWQgdG8gc2lnbiBpbiBvciBz\naWduIHVwIGJlZm9yZSBjb250aW51aW5nLgY7AFQ6CUBub3cwSSITdXNlcl9y\nZXR1cm5fdG8GOwBGIgYvSSIQX2NzcmZfdG9rZW4GOwBGSSIxWFpEVzZ0NWJo\nb3QxcWxReGpkQmplWDFMUSs4ZFdVbzU5VUFPNmxhSVRXND0GOwBG\n','2012-04-06 18:22:40','2012-04-06 18:22:40'),(8,'7d4fbde63e68227ebc7da6e7da44f3c6','BAh7B0kiEF9jc3JmX3Rva2VuBjoGRUZJIjE1R0pmK0JwNStwcXJuOXZjRlF1\nOVdnWnFqZlFLQ21LT0lFQzNaM1VLMFA4PQY7AEZJIhl3YXJkZW4udXNlci51\nc2VyLmtleQY7AFRbCEkiCVVzZXIGOwBGWwZpBkkiIiQyYSQxMCRReHBaTDdF\nQ2pVVUhWV3hUYUc1UHR1BjsAVA==\n','2012-04-07 06:26:43','2012-04-07 06:30:20'),(10,'bf2ce603e9282bcbb8a5ae72b7e240d5','BAh7B0kiEF9jc3JmX3Rva2VuBjoGRUZJIjE0d0JBQVprcHJISUxxb0w4NGhy\nNVRWMVJsMmpHaGVCdDVzcEIwUHRSeGNVPQY7AEZJIhl3YXJkZW4udXNlci51\nc2VyLmtleQY7AFRbCEkiCVVzZXIGOwBGWwZpBkkiIiQyYSQxMCRReHBaTDdF\nQ2pVVUhWV3hUYUc1UHR1BjsAVA==\n','2012-04-09 05:06:18','2012-04-09 05:06:23');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sticker_templates`
--

DROP TABLE IF EXISTS `sticker_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sticker_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` tinyint(1) DEFAULT NULL,
  `logo_bg_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrcode` tinyint(1) DEFAULT '1',
  `qrcode_bg_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer` tinyint(1) DEFAULT NULL,
  `footer_bg_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `logo_width` int(11) DEFAULT NULL,
  `logo_height` int(11) DEFAULT NULL,
  `qrcode_width` int(11) DEFAULT NULL,
  `qrcode_height` int(11) DEFAULT NULL,
  `notes` tinyint(1) DEFAULT NULL,
  `unit` tinyint(1) DEFAULT NULL,
  `note_font` int(11) DEFAULT NULL,
  `unit_font` int(11) DEFAULT NULL,
  `note_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note_width` int(11) DEFAULT NULL,
  `note_height` int(11) DEFAULT NULL,
  `unit_height` int(11) DEFAULT NULL,
  `unit_width` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sticker_templates`
--

LOCK TABLES `sticker_templates` WRITE;
/*!40000 ALTER TABLE `sticker_templates` DISABLE KEYS */;
INSERT INTO `sticker_templates` VALUES (1,'2012-04-06 16:41:15','2012-04-06 18:04:53','Template-1',1,NULL,1,NULL,1,NULL,'\r\n  \r\n<div class=\"template ui-resizable\" id=\"container\" style=\"top: 0px; left: 0px; width: 348px; height: 242px; \">\r\n    <div id=\"logo\" class=\"logo ui-widget-content ui-resizable ui-draggable\" style=\"width: 150px; height: 150px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 8px; top: 13px; \">\r\n       {{{ LOGO }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n\r\n    <div class=\"qrcode ui-resizable ui-draggable\" id=\"qrcode\" style=\"width: 150px; height: 150px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 179px; top: -139px; \">\r\n       {{{ QRCODE }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n \r\n        <div class=\"notes ui-resizable ui-draggable\" id=\"notes\" style=\"width: 231px; height: 33px; font-size: 12px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 9px; top: -118px; \">\r\n          {{{ NOTES }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n        <div class=\"unit ui-resizable ui-draggable\" id=\"unit\" style=\"width: 82px; height: 37px; font-size: 12px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 251px; top: -153px; \">\r\n          {{{ UNIT }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n <div class=\"ui-resizable-handle ui-resizable-e ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-s ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-draggable\" style=\"z-index: 1001; \"></div></div> \r\n\r\n\n\r\n\r\n\r\n\r\n\r\n',348,242,150,150,150,150,1,1,12,12,'center','center','Spongebob.jpg',231,33,37,82),(2,'2012-04-06 16:47:13','2012-04-06 18:07:07','Template 2',1,NULL,1,NULL,1,NULL,'\r\n  \r\n<div class=\"template ui-resizable\" id=\"container\" style=\"top: 0px; left: 0px; width: 412px; height: 300px; \">\r\n    <div id=\"logo\" class=\"logo ui-widget-content ui-resizable ui-draggable\" style=\"width: 159px; height: 154px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 6px; top: 15px; \">\r\n       {{{ LOGO }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n\r\n    <div class=\"qrcode ui-resizable ui-draggable\" id=\"qrcode\" style=\"width: 200px; height: 148px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 197px; top: -141px; \">\r\n       {{{ QRCODE }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n \r\n        <div class=\"notes ui-resizable ui-draggable\" id=\"notes\" style=\"width: 293px; height: 100px; font-size: 13px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 6px; top: -126px; \">\r\n          {{{ NOTES }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n        <div class=\"unit ui-resizable ui-draggable\" id=\"unit\" style=\"width: 87px; height: 98px; font-size: 12px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 309px; top: -228px; \">\r\n          {{{ UNIT }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n <div class=\"ui-resizable-handle ui-resizable-e ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-s ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-draggable\" style=\"z-index: 1001; \"></div></div> \r\n\r\n\n\r\n\r\n\r\n\r\n\r\n',412,300,159,154,200,148,1,1,13,12,'center','center','Spongebob.jpg',293,100,98,87),(3,'2012-04-06 18:09:48','2012-04-06 18:09:48','Template-3',1,NULL,1,NULL,1,NULL,NULL,500,500,100,100,150,150,0,0,15,12,'center','center','Spongebob.jpg',300,100,50,50),(4,'2012-04-06 18:10:27','2012-04-06 18:12:48','Template-4',1,NULL,1,NULL,1,NULL,'\r\n  \r\n<div class=\"template ui-resizable\" id=\"container\" style=\"top: 0px; left: 0px; width: 422px; height: 224px; \">\r\n    <div id=\"logo\" class=\"logo ui-widget-content ui-resizable ui-draggable\" style=\"width: 180px; height: 147px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 4px; top: 4px; \">\r\n       {{{ LOGO }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n\r\n    <div class=\"qrcode ui-resizable ui-draggable\" id=\"qrcode\" style=\"width: 209px; height: 150px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 193px; top: -147px; \">\r\n       {{{ QRCODE }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n \r\n        <div class=\"notes ui-resizable ui-draggable\" id=\"notes\" style=\"width: 300px; height: 37px; font-size: 15px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 4px; top: -139px; \">\r\n          {{{ NOTES }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n        <div class=\"unit ui-resizable ui-draggable\" id=\"unit\" style=\"width: 101px; height: 36px; font-size: 12px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 308px; top: -178px; \">\r\n          {{{ UNIT }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n <div class=\"ui-resizable-handle ui-resizable-e ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-s ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-draggable\" style=\"z-index: 1001; \"></div></div> \r\n\r\n\n\r\n\r\n\r\n\r\n\r\n',422,224,180,147,209,150,1,1,15,12,'center','center','Spongebob.jpg',300,37,36,101),(5,'2012-04-06 18:15:09','2012-04-06 18:21:37','Template-5',1,NULL,1,NULL,1,NULL,'\r\n  \r\n<div class=\"template ui-resizable\" id=\"container\" style=\"width:403px;height:264px;\">\r\n    <div id=\"logo\" class=\"logo ui-widget-content ui-resizable ui-draggable\" style=\"width: 169px; height: 149px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 20px; top: 17px; \">\r\n       {{{ LOGO }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n\r\n    <div class=\"qrcode ui-resizable ui-draggable\" id=\"qrcode\" style=\"width: 166px; height: 150px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 218px; top: -134px; \">\r\n       {{{ QRCODE }}}\r\n    <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n \r\n        <div class=\"notes ui-resizable ui-draggable\" id=\"notes\" style=\"width: 268px; height: 51px; font-size: 15px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 9px; top: -115px; \">\r\n          {{{ NOTES }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n        <div class=\"unit ui-resizable ui-draggable\" id=\"unit\" style=\"width: 100px; height: 48px; font-size: 18px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: relative; left: 290px; top: -166px; \">\r\n          {{{ UNIT }}}\r\n        <div class=\"ui-resizable-handle ui-resizable-e\"></div><div class=\"ui-resizable-handle ui-resizable-s\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 1001; \"></div></div>\r\n <div class=\"ui-resizable-handle ui-resizable-e ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-s ui-draggable\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-draggable\" style=\"z-index: 1001; \"></div></div> \r\n\r\n\n\r\n\r\n\r\n\r\n\r\n',403,264,169,149,166,150,1,1,15,18,'center','center','Spongebob.jpg',268,51,48,100),(6,'2012-04-07 06:30:19','2012-04-07 06:33:31','Template-6',1,NULL,1,NULL,1,NULL,NULL,416,301,178,153,162,150,1,1,15,20,'center','center','Spongebob.jpg',299,100,99,76);
/*!40000 ALTER TABLE `sticker_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stickers`
--

DROP TABLE IF EXISTS `stickers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stickers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sticker_template_id` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stickers`
--

LOCK TABLES `stickers` WRITE;
/*!40000 ALTER TABLE `stickers` DISABLE KEYS */;
INSERT INTO `stickers` VALUES (14,2,2,'2012-04-09 05:07:11','2012-04-09 05:07:11');
/*!40000 ALTER TABLE `stickers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Vanita','Kothari','vanita.kothari@idyllic-software.com','$2a$10$QxpZL7ECjUUHVWxTaG5Ptu5GjhES0N1T5QNM5ARtB66VmNc/DO3Wi',NULL,NULL,NULL,4,'2012-04-09 05:06:18','2012-04-07 06:26:43','120.62.179.141','120.62.182.51','2012-04-06 13:09:50','2012-04-09 05:06:18',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `validation_errors`
--

DROP TABLE IF EXISTS `validation_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `validation_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_encountered` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `validation_errors`
--

LOCK TABLES `validation_errors` WRITE;
/*!40000 ALTER TABLE `validation_errors` DISABLE KEYS */;
/*!40000 ALTER TABLE `validation_errors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-09  6:16:36
