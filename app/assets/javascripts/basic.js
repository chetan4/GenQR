// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs

//= require jquery.ui.draggable
//= require jquery.ui.droppable
//= require jquery.ui.resizable
//= require_self

$(document).ready(function() {
   $('div.template').resizable({ 
    stop : function(event,ui) {
     var width = $(this).width();
     var height = $(this).height();
     $("#template_width").val(width);
     $("#template_height").val(height);
    }

  })
  
  $("div.logo").resizable({
      stop : function(event,ui) {
        var width = $(this).width();
        var height = $(this).height();
        var klass = $(this).attr('id');
        $("#"+klass+"_width").val(width);
        $("#"+klass+"_height").val(height);
      },
      containment: "#container"
		
	}); 
	
	  $("div.qrcode").resizable({
      stop : function(event,ui) {
        var width = $(this).width();
        var height = $(this).height();
        var klass = $(this).attr('id'); 
        $("#"+klass+"_width").val(width);
        $("#"+klass+"_height").val(height);
      },
      containment: "#container"
		
	}); 

   $("div.notes").resizable({
      stop : function(event,ui) {
        var width = $(this).width();
        var height = $(this).height();
        var klass = $(this).attr('id');
        $("#"+klass+"_width").val(width);
        $("#"+klass+"_height").val(height);
      },
			containment: "#container"
	}); 
   
   $("div.unit").resizable({
      stop : function(event,ui) {
        var width = $(this).width();
        var height = $(this).height();
        var klass = $(this).attr('id');
       
        $("#"+klass+"_width").val(width);
        $("#"+klass+"_height").val(height);
      },
		 	containment: "#container"
	}); 
   
    $("div.serial_number").resizable({
      stop : function(event,ui) {
        var width = $(this).width();
        var height = $(this).height();
        var klass = $(this).attr('id');
       
        $("#"+klass+"_width").val(width);
        $("#"+klass+"_height").val(height);
      },
      containment: "#container"
  }); 
  $("div.template > *").draggable({
    containment: "#container"
  });
  
  $('.sticker').bind("ajax:success",function(evt, data, status, xhr){
    window.parent.call_me(data);
  })
});
	    
function send_request() {
    $('.template_body').val($('body').html());
    $('.click_me').click();
}
