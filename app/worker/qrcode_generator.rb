class QrcodeGenerator
  @queue = :qr_code_generator
  ## IN File Duplication need to be resolved
  def self.perform(record_id,klass=nil)
    Rails.logger.info "Generating QRCode for the record in files ..."

    products = klass.nil? ? Asset.find(record_id).products : Product.where('id=?',record_id) 

    products.each do |product| 
      product.make_webservice_call
    end

    if klass.nil?
      Asset.find(record_id).update_column('processed',true)
    end  
    Rails.logger.info "Savely Generated QRCode for the record in the files !!!"
  end
  

end
