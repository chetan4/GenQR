class RegenerateQrCode
	@queue = :regenerate_qrcode

	def self.perform()
		Rails.logger.info "Regenerating QRcode that could not be generated on first time"
		products = Product.where(:processed => false).where(Product.arel_table[:error_message].not_eq(nil))
		products.each do |product|
			product.make_webservice_call
		end 
		Rails.logger.info "Process of QRcode Regeneration has completed"
	end
end