class StickerTemplate < ActiveRecord::Base
  has_many :stickers ,:dependent => :destroy
  has_many :assets,:through => :stickers
  validates :name,:presence => true,:uniqueness => {:scope => :user_id }
  validates :width,:height,:presence => true
  
  validates :width,:numericality => { :only_integer => true,:message => "must be number" },:unless => "width.nil?"
  
  validates :height,:numericality => { :only_integer => true,:message => "must be number" },:unless => "height.nil?"
  
  validates :logo_image,:logo_width,:logo_height,:presence => true,:if => :logo_exists?
  
  validates :logo_width,:numericality => {:only_integer => true ,:message => "must be number"},:if => :logo_exists_and_width_nil? 
  
  validates :logo_height,:numericality => {:only_integer => true ,:message => "must be number"},:if => :logo_exists_and_height_nil?
  
  validates :qrcode_width,:qrcode_height ,:presence => true
  
  validates :qrcode_width,:numericality => { :only_integer => true ,message: "must be number"} ,:unless => "qrcode_width.nil?"
  
  validates :qrcode_height,:numericality => { :only_integer => true ,message: "must be number"} ,:unless => "qrcode_height.nil?"
  
  validates :unit_font,:presence => true,:if => :unit_exists?
  
  validates :unit_font ,:numericality => { :only_integer => true ,:message => "must be number"} ,:if => "unit? && !unit_font.nil?"
  
  validates :note_font,:presence => true,:if => :note_exists?
  
  validates :note_font ,:numericality => { :only_integer => true ,:message => "must be number"},:if => "notes? && !note_font.nil?"
  
  validates :serial_number_font,:presence => true,:if => :serial_number_exists?
  
  validates :serial_number_font ,:numericality => { :only_integer => true ,:message => "must be number"},:if => "serial_number? && !serial_number_font.nil?"
  
  validate :validate_qrcode_width_height ,:unless => "(qrcode_height.nil? || qrcode_width.nil?)"
  
  belongs_to :user 
 
  mount_uploader :logo_image ,LogoUploader
  Position = {"Left" => "left","Center" => "center","Right" => "right" }
  before_save :remove_form_tag ,:on => :update
 
  after_save :resize_image ,:if => :logo_parameter_changed?
  before_save :set_note_container , :if => "notes? && note_width.nil? && note_height.nil?"
  before_save :set_unit_container,:if => "unit? && unit_width.nil? && unit_height.nil?"
  FontStyle = %w(Arial Thamo)
  
  scope :default_sticker_template ,where("default_template = ?",true)

  DefaultOptions = {
    :default_template => true,
    :width  =>  500, 
    :height => 240,
    :qrcode => true,
    :qrcode_width => 218,
    :qrcode_height => 171,
    :logo   =>  false,
    :footer => true,
    :notes => true,
    :note_width => 200,
    :note_height => 21,
    :note_font => 10,
    :note_font_style =>"Arial",
    :note_position => "center",
    :unit  => true,
    :unit_width => 226,
    :unit_height => 17,
    :unit_font => 15,
    :unit_font_style => "Thamo",
    :unit_position => "left",
    :body   => <<BODY 
    
    <div class="template ui-resizable" id="container" style="top: 0px; left: 0px; width: 500px; height: 240px; ">

    <div class="qrcode ui-resizable ui-draggable" id="qrcode" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: absolute; width: 218px; height: 171px; left: 124px; top: 10px; ">
       {{{ QRCODE }}}
    <div class="ui-resizable-handle ui-resizable-e"></div><div class="ui-resizable-handle ui-resizable-s"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 1001; "></div></div>
 
        <div class="notes ui-resizable ui-draggable" id="notes" style="font-size: 10px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: absolute; width: 200px; height: 21px; left: 20px; top: 197px; ">
          {{{ NOTES }}}
        <div class="ui-resizable-handle ui-resizable-e"></div><div class="ui-resizable-handle ui-resizable-s"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 1001; "></div></div>
        <div class="unit ui-resizable ui-draggable" id="unit" style="font-size: 15px; text-align: left; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; position: absolute; width: 226px; height: 17px; left: 236px; top: 198px; ">
          {{{ UNIT }}}
        <div class="ui-resizable-handle ui-resizable-e"></div><div class="ui-resizable-handle ui-resizable-s"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 1001; "></div></div>
 <div class="ui-resizable-handle ui-resizable-e ui-draggable"></div><div class="ui-resizable-handle ui-resizable-s ui-draggable"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-draggable" style="z-index: 1001; "></div></div> 
BODY
  }
  
  def remove_form_tag
   if !body.nil?
    body.gsub!(/<form.+/,"")  unless  body.scan(/<form.+/).empty? 
   end 
  end
  
  
  private
   
  def logo_exists?
    self.logo
  end
  
  def unit_exists?
    self.unit
  end
  
  def note_exists?
    self.notes
  end

  def serial_number_exists?
    self.serial_number
  end
  
  def logo_parameter_changed?
    self.logo && (self.logo_width_changed? || self.logo_height_changed?) 
  end
  
  def resize_image
    img = ImageList.new(File.join(Rails.root,'public',self.logo_image.to_s))
    img.resize!(self.logo_width.to_i, self.logo_height.to_i)
    img.write(File.join(Rails.root,'public',self.logo_image.to_s))
  end

  def validate_qrcode_width_height
    self.errors[:qrcode] << "Maximum Allocated size for QRCODE is 500x500" if self.qrcode_width > 500 || self.qrcode_height > 500
  end
  
  def set_note_container
    self.note_width = (self.width.to_i * 0.4).to_i
    self.note_height = self.note_font.to_i + 2
  end
  
  def set_unit_container
    self.unit_width = (self.width.to_i * 0.4).to_i
    self.unit_height = self.unit_font.to_i + 2
  end
  
  def logo_exists_and_width_nil? 
     self.logo && !self.logo_width.nil?
  end
  
  def logo_exists_and_height_nil? 
    self.logo && !self.logo_height.nil?
  end
  
end
