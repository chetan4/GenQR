class Asset < ActiveRecord::Base
  has_many :products,:dependent => :destroy
  has_many :validation_errors ,:dependent => :destroy
  validates :name,:asset,:presence => true 
  has_many :stickers ,:dependent => :destroy
  has_many :sticker_templates,:through => :stickers
  belongs_to :user
  mount_uploader :asset,AssetUploader

  scope :processing_qrcode , where('status =? and processed =?','successful',false) 
  
  def failed?
    self.status == "failed"
  end
  
  def clear_validation_errors
    self.validation_errors.destroy_all
  end  
  
  
  def post_process
    download_file_from_s3_and_process
  end
  
  def create_stickers
    self.stickers.create(:sticker_template_id => default_template.id)
  end
  
  def filename
    asset.filename_with_pdf
  end
  
 private  
 
  def default_template
    StickerTemplate.where('name=? and user_id=?',"Default Template",self.user_id).first 
  end

  def download_file_from_s3_and_process
    begin
      create_directory
      download_file
      process_the_csv_file
    rescue => e
      Rails.logger.info "Failed Citing error #{e}"
      self.update_attributes(:status => 'failed',:error_encountered => e.to_s,:uploaded_at => DateTime.now)
    ensure
      remove_directory
    end
  end
 
  def process_the_csv_file
    product_attributes = []
    product_value = [] 
    product_hash  = []
    validation_errors  = []
    make_model_serial_combination = []
    vin_no = []
    row_no = 0
    make_index = 0
    model_index = 0
    serial_number_index = 0
    vin_index = 0
    times = 0
    
    
    CSV.foreach(File.join($DOWNLOAD_DIRECTORY,id.to_s,self.asset.filename.to_s)) do |row|
      row_no +=1
      row_records = row.collect { |record| record.strip unless record.nil? }
      if (row.empty? || row_records.length == 1 && row_records[0].empty?)
        next  
      end
      
      if product_attributes.empty?
        product_attributes = row.collect(&:strip).collect(&:downcase)
        vin_index = product_attributes.index('vin')
        make_index = product_attributes.index('make')
        model_index = product_attributes.index('model')
        serial_number_index = product_attributes.index('serial_number')
        next
      end
      
      record_hash = Hash.new
      
      row.each_with_index do |record,index|
        record_hash.merge!(product_attributes[index].to_sym => record)  
      end
      
      record_hash.merge!(:asset_id => self.id,:user_id => self.user_id)
      
      product = Product.new(record_hash)
     
      if product.valid? && (row.length == product_attributes.length) && (row[vin_index].nil? ? !make_model_serial_combination.compact.include?(row[make_index].to_s+row[model_index].to_s+row[serial_number_index].to_s) : !vin_no.compact.include?(row[vin_index]))   
        product_hash << row.push(self.id).push(self.user_id)
        make_model_serial_combination << row[make_index].to_s+row[model_index].to_s+row[serial_number_index].to_s
        vin_no << row[vin_index] 
      else
        times += 1
        product.errors.add :base, 'Entire row is blank' if !row.join.gsub!(',','').present?
        product.errors.add :base,"The given rows values doesnot match the attributes supplied" if row.length != product_attributes.length 
        #product.errors.add :vin,"There is already this 'vin' present in the current file" if !row[vin_index].nil? && vin_no.compact.include?(row[vin_index])
        #product.errors.add :base,"The combination of make,model,serial_number is not unique i.e already present" if row[vin_index].nil? && make_model_serial_combination.compact.include?(row[make_index].to_s+row[model_index].to_s+row[serial_number_index].to_s)
        validation_errors << [row_no,product.errors.messages.to_yaml,self.id]     
      end
      
      row_records,product,record_hash=[nil,nil,nil]  
    end
    if times.zero?
    
      Product.import(product_attributes.push(:asset_id).push(:user_id),product_hash,:validate => false)
      self.update_attributes(:status => 'successful',:uploaded_at => DateTime.now)
      Rails.logger.info "Resque Command Initiated ........................." 
      Resque.enqueue(QrcodeGenerator,self.id)
      Rails.logger.info "Resque Command Running in Background ...................."
    else
      self.update_attributes(:status => 'failed',:uploaded_at => DateTime.now)
      ValidationError.import([:row_id,:error_encountered,:asset_id],validation_errors,:validate => false)
    end
    
  end
  
  
  def download_file
    Rails.logger.info "Downloading file from S3 ..."
    File.open(File.join($DOWNLOAD_DIRECTORY,id.to_s,self.asset.filename.to_s),'w+') do |file|
     file <<  $storage.directories.get($BUCKET).files.get(File.join(self.asset.store_dir,self.asset.filename)).body.force_encoding("UTF-8")
    end 
    Rails.logger.info "Downloaded file from S3 !!!"
  end
  
  def create_directory
    Rails.logger.info "Creating Directory ..."
    unless File.exists?(File.join($DOWNLOAD_DIRECTORY,id.to_s))
      FileUtils.mkdir_p File.join($DOWNLOAD_DIRECTORY,id.to_s)
    end  
    Rails.logger.info "Created Directory !!!"
  end
  
  def remove_directory
    Rails.logger.info "Removing Directory ..."  
    FileUtils.rm_r File.join($DOWNLOAD_DIRECTORY,id.to_s) if File.exists?(File.join($DOWNLOAD_DIRECTORY,id.to_s))
   Rails.logger.info "Removed Directory !!!"  
  end
  
  
 
end
