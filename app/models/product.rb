class Product < ActiveRecord::Base

 
  belongs_to :asset
  belongs_to :user
  validates :make,:model,:serial_number,:vin ,:format => { :with => /^[a-z0-9A-Z\s*-]+[-a-z0-9\s-]*[a-z0-9\s*-]+$/i ,:message => "Only AlphaNumeric Allowed" },:allow_nil => true,:on=> :create
  #validation of make+model+serial_number combination in database and file not done
  
  validates :vin,:length => {:maximum => 17 ,:message => "Maximum length allowed is 17"},:unless => "vin.nil?" , :allow_nil => true ,:on => :create
  #validates :vin ,:uniqueness => {:scope => :user_id} ,:unless => "vin.nil?" ,:on => :create
  
 # validate :combination_vin,:if => "vin.nil?",:on => :create
  ## Editing from Gihub  
  scope :qr_code_generation_failed , where('processed =?',false) 
  def make_webservice_call
    url = if vin.present? 
            initialize_url(WEBSERVICE % "vin=#{vin}")
          else
            initialize_url(WEBSERVICE % "make=#{make}&model=#{model}&serial=#{serial_number}")
          end   
    http = initialize_http(url)
    response = http.get(url.path+"?"+url.query)  
    extract_url(response)
  end
  
  def qr_codeg(url)
    update_attributes(:qrcode_url => url,:processed => true,:error_message => nil)
  end
  
  private
  
  def initialize_url(url)
    Rails.logger.info "Initializing URL..............."
    URI.parse url
  end
  
  def initialize_http(url)
    Rails.logger.info "Initializing HTTP ..............."
    Net::HTTP.new(url.host,url.port)
  end
  
  def extract_url(response)
    if response.code == "200" && JSON.parse(response.body)["url"].present?
       qr_codeg(JSON.parse(response.body)["url"])
    elsif response.code == "200" && JSON.parse(response.body)["error"].present?
       error_message = []
       JSON.parse(response.body)["error"].each do |key,value| 
         error_message << "#{key} : #{value}"
       end
       mark_error_for_product(error_message.join(",")) 
    else
       mark_error_for_product("Web Service Could be down")
    end
  end

  def mark_error_for_product(error_message)
    update_attributes(:processed => false,:error_message => error_message)
  end

  
  def vin_number
    vin.present? ? vin : make+model+serial_number
  end
  
  
  private
  
  def record_presence
    if make.nil? && model.nil? && serial_number.nil? && vin.nil? && unit.nil? && notes.nil?
      errors.add(:base,"All fields are blank.")
      return
    end
  end

  def combination_vin
    if make.nil? && model.nil? && serial_number.nil?
      errors.add(:base,"Make sure vin if not present make,model,serial_number should be present")
      return
    end
        
    if Product.exists?(:make => make,:model => model,:serial_number => serial_number,:user_id => user_id) 
      errors.add(:base,"The Combination of 'make+model+serial_number' already present")
    end
 
  end
  

end

 
