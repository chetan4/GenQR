class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :first_name,:last_name,:email, :password, :password_confirmation, :remember_me,:admin
  validates :first_name ,:presence => true
  after_create :create_default_templates
  has_many :assets
  has_many :sticker_templates
  has_many :products
  def name
    first_name+" "+last_name
  end
  
  def create_default_templates
    self.sticker_templates.create({:name => "Default Template"}.merge(StickerTemplate::DefaultOptions))
  end
  
end
