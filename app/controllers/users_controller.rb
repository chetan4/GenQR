class UsersController < ApplicationController
  authorize_resource
  
  def index
    @users = User.order('created_at desc')
  end
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    password = params[:user][:password]
    username = params[:user][:username]
    respond_to do |format|
      if @user.save
        Email.user_creation(@user,password).deliver
        flash[:message] = "User Successfully Created"
        format.html { redirect_to users_path }
      else
        format.html { render action: "new" }
      end
    end
  end  

  def edit
    @user = User.find(params[:id])
    authorize! :edit, @user 
  end

  def update
    @user = User.find(params[:id])
    authorize! :update, @user
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    
    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:message] = "User Updated Successfully"
          if (current_user.id == @user.id)
            sign_in(@user, :bypass => true)
          end   
          if current_user.admin?
            format.html { redirect_to users_path }
          else
            format.html { redirect_to home_path }
        end  
      else
        format.html { render action: "edit" }
      end 
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path }
    end
  end
end
