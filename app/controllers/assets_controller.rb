class AssetsController < ApplicationController

  authorize_resource
  before_filter :load_asset ,:only => [:edit,:update,:show,:destroy,:all_sticker,:download_sticker,:validation_error]
  #layout 'sticker',:only => [:all_sticker,:download_sticker]
  layout 'sticker',:only => [:download_sticker]
  def index
    
    @assets = current_user.assets.where('status=?','successful').order('updated_at desc').includes('sticker_templates')
    @sticker_templates = current_user.sticker_templates
  end
  
  def failed_asset
    @assets = current_user.assets.where('status=?','failed').order('updated_at desc')
    respond_to do |format|
      if request.xhr?
        format.js { render :partial => 'failed_asset' }
      else
        format.html 
      end
    end
  end
  
  def create
    @asset = current_user.assets.new(params[:asset])
    respond_to do |format|
      if @asset.save
        flash[:message] = "Asset Create Successfully"
        @asset.post_process
        if @asset.failed?
          flash[:notice] = 'Error Uploading Asset'
          flash[:klass] = 'alert-error' 
          format.html { redirect_to failed_asset_assets_path }
        else
          @asset.create_stickers
          flash[:notice] = 'Successfully Uploaded Asset'
          flash[:klass] = 'alert-success'
          format.html { redirect_to assets_path } 
        end  
      else
        format.html { render action: 'new' }
      end
    end  
  end

  def new
    @asset = current_user.assets.new
  end
  
  def edit 
   
  end

  def validation_error
    @validation_errors  = @asset.validation_errors
    respond_to do |format|
      format.js { render :partial => 'validation_error' }
    end
  end
  
  def update
    respond_to do |format|
      if @asset.update_attributes(params[:asset])
        flash[:message] = "Asset Updated Successfully"
        @asset.clear_validation_errors
        @asset.post_process
        if @asset.failed?
          flash[:notice] = 'Error Uploading Asset' 
          flash[:klass] = 'alert-error'
          format.html { redirect_to failed_asset_assets_path}
        else
          flash[:notice] = 'Successfully Uploaded Asset'
          flash[:klass] = 'alert-success'
          format.html { redirect_to assets_path}
        end  
      else
        format.html { render action: "edit" }
      end 
    end
  end

  def show
    @products = @asset.products
  end  
  
  
  def destroy
    respond_to do |format|
     redirect_page = @asset.failed?
      if @asset.destroy
       flash[:notice] = 'Successfully Destroyed Asset'
       flash[:klass] = 'alert-success' 
       if redirect_page
         format.html { redirect_to failed_asset_assets_path }
       else
        format.html { redirect_to assets_path}
       end 
      end
    end
  end

  def remove_template
    @asset = Asset.find(params[:id])
    respond_to do |format|
      @asset.stickers.destroy_all
      @asset.stickers.create(:sticker_template_id => params[:sticker_template_id])
      format.html { render text: "Successfully Applied Template" }
    end  
  end
  
  def print_sticker
    @asset = Asset.find(params[:id])
  end
 
  
  def all_sticker
    @product = @asset.products
    @sticker = @asset.stickers.first
  end
  
  
  
  def download_sticker
    @product = params[:product_id].present? ? [@asset.products.find(params[:product_id])] : @asset.products
    @printer = params[:printer]
    @page_break = true if params[:page_break].present?
    @sticker = @asset.stickers.first
    send_data(to_pdf,:filename => @asset.filename,:type => "application/pdf")
  end
  
  private
  
  def load_asset
    @asset = current_user.assets.find(params[:id])
  end
  
  def to_pdf
    invoke = execute_command
    create_cookie_jar
    Rails.logger.info %Q{#{render_to_string(:layout => true).gsub('"', "'")}}
    result = IO.popen(invoke, "wb+") do |pdf|
      pdf.puts(%Q{#{render_to_string(:layout => true).gsub('"', "'")}})
      pdf.close_write
      pdf.gets(nil)
    end
    remove_cookie_jar
    return result
  end

 
  
  def binary_path
    WkhtmlToPdf
  end
  
  def execute_command
    binary_path + options 
  end
  
  def pdfkit_options
    case @printer
    when "A"
      {:print_media_type => true, :cookie_jar => "#{create_cookie_jar}", :dpi => 300, :margin_top => 0, :margin_right => 0, :margin_bottom => 0, :margin_left => 0, :page_width => 18, :page_height => 27, :custom_header => "cookie #{cookies}" }
    when "B"
      {:print_media_type => true, :cookie_jar => "#{create_cookie_jar}", :dpi => 300, :margin_top => 0, :margin_right => 0, :margin_bottom => 0, :margin_left => 0, :page_width => 58, :page_height => 87, :custom_header => "cookie #{cookies}" }
    else
      {:print_media_type => true, :cookie_jar => "#{create_cookie_jar}", :dpi => 300, :margin_top => 0, :margin_right => 0, :margin_bottom => 0, :margin_left => 0, :custom_header => "cookie #{cookies}" }
    end
  end
    
  def options
    if @printer.nil?
      return " --cookie-jar '#{create_cookie_jar}' --custom-header 'cookie' '#{cookies}' --dpi 300  --margin-top 0 --margin-bottom 0 --margin-right 0 --margin-left 0 --quiet - -"
    elsif @printer == "A"
      return " --cookie-jar '#{create_cookie_jar}' --custom-header 'cookie' '#{cookies}' --dpi 300  --page-width 18 --page-height 27 --margin-top 0 --margin-bottom 0 --margin-right 0 --margin-left 0 --quiet - -"
    elsif @printer == "B"
      return " --cookie-jar '#{create_cookie_jar}' --custom-header 'cookie' '#{cookies}' --dpi 300  --page-width 58 --page-height 87 --margin-top 0 --margin-bottom 0 --margin-right 0 --margin-left 0 --quiet - -"
    end 
  end
  
  def cookies
   cookie_value =  request.cookies[cookie_name]
   "#{cookie_name}=#{cookie_value}"
  end
  
  def cookie_name
    key = "_session_id"
    key = Rails.configuration.session_options[:key] if Rails.configuration.session_store.name == "ActionDispatch::Session::CookieStore"
    key
  end
  
  def create_cookie_jar
    unless File.exists?(File.join(Rails.root,'public',"#{current_user.first_name}_#{current_user.id}_cookies.wkhtmltopdf.txt"))
      File.open(File.join(Rails.root,'public',"#{current_user.first_name}_#{current_user.id}_cookies.wkhtmltopdf.txt"),'w+') do |file|
       file.write cookies+"; HttpOnly; domain=#{DOMAIN}; path=/;"
      end 
    end 
    File.join(Rails.root,'public',"#{current_user.first_name}_#{current_user.id}_cookies.wkhtmltopdf.txt")
  end
  
  def remove_cookie_jar
    File.delete File.join(Rails.root,'public',"#{current_user.first_name}_#{current_user.id}_cookies.wkhtmltopdf.txt")
  end
end
