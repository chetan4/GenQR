#require "open-uri"
class ProductsController < ApplicationController
  authorize_resource
  
  before_filter :load_product ,:only => :show
  
  
  def show  
    #raise "ERROR" if @product.nil?
    @sticker = Sticker.find(params[:sticker_id])
  end


  def regenerate_qrcode
    @product = Product.find(params[:id])
    Resque.enqueue(QrcodeGenerator,@product.id,"Product")
    respond_to do |format|
     format.html {  redirect_to asset_path(@product.asset) }
    end
  end
 
  private
  
    def load_product
      @product = current_user.products.find(params[:id])
    end
  
end 
