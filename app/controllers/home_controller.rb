class HomeController < ApplicationController
  skip_authorization_check :only => [:index,:thank_you]

  def index

  end
  
  def thank_you
  
  end
end
