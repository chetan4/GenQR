class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!
  check_authorization :unless => :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to home_url, :alert => exception.message
  end



  private
  def after_sign_in_path_for(resource)
    home_path
  end
  
  def after_sign_out_path_for(resource)
    login_path
  end
end
