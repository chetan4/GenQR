class StickersController < ApplicationController

  authorize_resource
  
  before_filter :load_asset ,:only => :create_sticker
  #layout 'sticker', :only => :show
  def create_sticker

    respond_to do |format|
      if @asset.sticker_templates.empty?
        @asset.stickers.create(:sticker_template_id => load_template.id)
        @achieved = true
      end
      format.js
    end 
  end
  
  def show
    @sticker = Sticker.find(params[:id])
    @product = Product.find(params[:product_id])
    
   if params[:layout].present?
      params[:layout] = false
    else
      params[:layout] = true
    end 
    
    respond_to do |format|
      format.html { render :layout => params[:layout]}
    end
  end
  
  private
  
  def load_asset
    @asset = Asset.find(params[:asset_id]) 
  end
  
  def load_template
    @template = StickerTemplate.find(params[:sticker_template_id])
  end
end
