class StickerTemplatesController < ApplicationController
  # GET /sticker_templates
  # GET /sticker_templates.json
  authorize_resource
  layout 'basic' ,:only => 'sample_template'
  def index
    @sticker_templates = current_user.sticker_templates

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sticker_templates }
    end
  end

  # GET /sticker_templates/1
  # GET /sticker_templates/1.json
  def show
    @sticker_template = current_user.sticker_templates.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sticker_template }
    end
  end

  # GET /sticker_templates/new
  # GET /sticker_templates/new.json
  def new
    @sticker_template = current_user.sticker_templates.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sticker_template }
    end
  end

  # GET /sticker_templates/1/edit
  def edit
    @sticker_template = current_user.sticker_templates.find(params[:id])
  end

  # POST /sticker_templates
  # POST /sticker_templates.json
  def create
    @sticker_template = current_user.sticker_templates.new(params[:sticker_template])

    respond_to do |format|
      if @sticker_template.save
        format.html { redirect_to @sticker_template, notice: 'Sticker template was successfully created.' }
        format.json { render json: @sticker_template, status: :created, location: @sticker_template }
      else
        format.html { render action: "new" }
        format.json { render json: @sticker_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sticker_templates/1
  # PUT /sticker_templates/1.json
  def update
    @sticker_template = current_user.sticker_templates.find(params[:id])

    respond_to do |format|
      if @sticker_template.update_attributes(params[:sticker_template].merge(:body => nil))
        format.html { redirect_to @sticker_template }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def update_copy
    @sticker_template = current_user.sticker_templates.find(params[:id])

    respond_to do |format|
      if @sticker_template.update_attributes(params[:sticker_template])
        format.js { render :text => 'Sticker Template is updated successfully' ,:layout => false}
      end
    end
  end
  
  # DELETE /sticker_templates/1
  # DELETE /sticker_templates/1.json
  def destroy
    @sticker_template = current_user.sticker_templates.find(params[:id])
    asset =  @sticker_template.assets.first unless @sticker_template.assets.empty?
    
    @sticker_template.destroy
    
    unless asset.nil?
      asset.stickers.create(:sticker_template_id => default_template.id)
    end
    respond_to do |format|
      format.html { redirect_to sticker_templates_url,notice: 'Asset Deleted',klass: 'success' }
      
    end
  end
  
  def sample_template
    @sticker_template = current_user.sticker_templates.find(params[:id])
  end
  
  private
  
  def default_template
    current_user.sticker_templates.default_sticker_template.first
  end
  
end
