class Email < ActionMailer::Base
  ## Change this with something of yours
  default from: "admin@genqr.com"
  
  def user_creation(user,password)
    @user =  user
    @password = password
    mail(:to => user.email,:subject => "GENQR Registration")
  end
  
end
