module ApplicationHelper

  def current_css_classes(css_class) 
    if css_class == container_classes
      return "active"
    else
      return ""
    end    
  end
  
  def container_classes
     pages = {
      "users#index"   => "user", 
      "users#create"  => "user",
      "users#new"     => "user",
      "users#edit"    => "user",
      "users#destroy" => "user",
      "home#index"    => "home",
      "home#thank_you" => "home",
      "products#show" => "asset",
      "assets#index"  => "asset",
      "assets#failed_asset" => "asset",
      "assets#create" => "asset",
      "assets#all_sticker" => "asset",
      "assets#update" => "asset",
      "assets#edit"   => "asset",
      "assets#show"   => "asset",
      "assets#new"    => "asset",
      "assets#print_sticker" => "asset",
      "sticker_templates#index" => "template",
      "sticker_templates#new" => "template",
      "sticker_templates#show" => "template",
      "sticker_templates#create" => "template",
      "sticker_templates#edit" => "template",
      "sticker_templates#update" => "template",
      "sticker_templates#destroy" => "template"
      
      
    } 
    
    pages["#{controller.controller_name}##{controller.action_name}"] 
  end
 
  def full_path(path=String.new)
    request.scheme+"://"+SERVER_DOMAIN+path
  end
  
  def qrimage(url=nil)
   #image_tag(qrcode_link(url))
   "<img alt='qrcode' src='#{raw qrcode_link(url)}' />"
  end
  
  def qrcode_link(url=nil)
    url ||= @product.qrcode_url
    GOOGLE_CHART_API % [@sticker.sticker_template.qrcode_width.to_i-10, @sticker.sticker_template.qrcode_height.to_i-10,CGI.escape(url)]
  end
  
  def logo_tag
    if @sticker.sticker_template.logo?
      image_tag(absolute_asset_path(@sticker.sticker_template.logo_image.to_s))
    end  
  end
  
  def content_error(tag,arry)
    wrapper = String.new
    if arry.is_a?(ActiveModel::Errors)
      arry.each do |error,description|
        wrapper += content_tag(tag,"#{error.capitalize} #{description}")
      end
    end
    return wrapper
  end

  def sticker_css_class
    @page_break ? "break" : "no-break" 
  end 

  def absolute_asset_path(path='')
    Rails.env=='development' ? path : request.scheme+"://"+SERVER_DOMAIN+path
  end


end
