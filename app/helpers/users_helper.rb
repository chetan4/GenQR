module UsersHelper

  def generate_message
    msg = '<h4> Following Error Occurred </h4> <br/><ol>'
    msg += format_message
    msg += '</ol>'      
   end
   
   def join_error_message(error,message)
      error.concat(' : ').concat(message.join(' '))
   end
   
   def format_message
     listing = ''
     @user.errors.messages.each do |error,message|
       listing += content_tag(:li,join_error_message(error.to_s,message))
     end   
     return listing
   end
   
   def insert_new_record
    %Q{"<tr id='#{dom_id(@user)}'><td>#{@user.name}</td><td><a href='/users/#{@user.id}/edit' class='editable' data-remote='true' data-type='html'><i class='icon-edit icon-black'></i></a> <a href='/users/#{@user.id}' data-confirm='Are you Sure You want to delete' data-method='delete' rel='nofollow'><i class='icon-trash icon-black'></i></a></td></tr>"}
   end
   
   def update_existing_record
     %Q{"<tr id='#{dom_id(@user)}'><td>#{@user.name}</td><td><a href='/users/#{@user.id}/edit' class='editable' data-remote='true' data-type='html'><i class='icon-edit icon-black'></i></a> <a href='/users/#{@user.id}' data-confirm='Are you Sure You want to delete' data-method='delete' rel='nofollow'><i class='icon-trash icon-black'></i></a></td></tr>"} 
   end
   
   def generate_id
     if controller.action_name == "edit" || controller.action_name == "update"
       return "editModal"
     else
       return "myModal"
     end  
   end
end


