module AssetsHelper 
  def generate_id
    if controller.action_name == "edit" || controller.action_name == "update"
      return "editModal"
    else
      return "myModal"
    end  
  end
  
  def generate_message
    msg = '<h4> Following Error Occurred </h4> <br/><ol>'
    msg += format_message
    msg += '</ol>'      
   end
   
   def join_error_message(error,message)
      error.concat(' : ').concat(message.join(' '))
   end
   
   def format_message
     listing = ''
     @asset.errors.messages.each do |error,message|
       listing += content_tag(:li,join_error_message(error.to_s,message))
     end   
     return listing
   end
   
   
   def selected_sticker_template(asset)
     asset.sticker_templates.first.id rescue nil
   end
   
   
   def display_error(error)
    error_obj = YAML.load(error.error_encountered)
    "<li style='padding-left:25px'> <label class='error_lbl4'>Row_id #{error.row_id} :- #{error_description(error_obj)} </label> </li>"
   end
  
  def error_description(obj)
    error_field = String.new

    obj.each do |key,value|
      error_field += "#{key} : #{value.join(',')}"
    end
    return error_field
  end
  
end
