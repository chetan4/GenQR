require 'test_helper'

class StickerTemplatesControllerTest < ActionController::TestCase
  setup do
    @sticker_template = sticker_templates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sticker_templates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sticker_template" do
    assert_difference('StickerTemplate.count') do
      post :create, sticker_template: @sticker_template.attributes
    end

    assert_redirected_to sticker_template_path(assigns(:sticker_template))
  end

  test "should show sticker_template" do
    get :show, id: @sticker_template
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sticker_template
    assert_response :success
  end

  test "should update sticker_template" do
    put :update, id: @sticker_template, sticker_template: @sticker_template.attributes
    assert_redirected_to sticker_template_path(assigns(:sticker_template))
  end

  test "should destroy sticker_template" do
    assert_difference('StickerTemplate.count', -1) do
      delete :destroy, id: @sticker_template
    end

    assert_redirected_to sticker_templates_path
  end
end
