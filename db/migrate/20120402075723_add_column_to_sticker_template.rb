class AddColumnToStickerTemplate < ActiveRecord::Migration
  def change
    add_column :sticker_templates, :name,:string
    add_column :sticker_templates, :logo, :boolean
    add_column :sticker_templates, :logo_bg_color ,:string
    add_column :sticker_templates, :qrcode, :boolean,:default => true
    add_column :sticker_templates, :qrcode_bg_color,:string
    add_column :sticker_templates, :footer, :boolean
    add_column :sticker_templates, :footer_bg_color,:string 
    add_column :sticker_templates, :body,:text
    add_column :sticker_templates,:width,:integer
    add_column :sticker_templates,:height,:integer
    add_column :sticker_templates,:logo_width,:integer
    add_column :sticker_templates,:logo_height,:integer
    add_column :sticker_templates,:qrcode_width,:integer
    add_column :sticker_templates,:qrcode_height,:integer
    add_column :sticker_templates,:notes,:boolean
    add_column :sticker_templates,:unit,:boolean
    add_column :sticker_templates,:note_font,:integer
    add_column :sticker_templates,:note_width,:integer
    add_column :sticker_templates,:note_height,:integer
    add_column :sticker_templates,:unit_width,:integer
    add_column :sticker_templates,:unit_height,:integer
    add_column :sticker_templates,:unit_font ,:integer
    add_column :sticker_templates,:note_position,:string
    add_column :sticker_templates,:unit_position,:string
    add_column :sticker_templates,:logo_image,:string
    add_column :sticker_templates,:user_id,:integer
  end
end
