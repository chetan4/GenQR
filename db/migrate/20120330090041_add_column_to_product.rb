class AddColumnToProduct < ActiveRecord::Migration
  def change
    add_column :products, :asset_id, :integer
    add_column :products, :qrcode_url, :text
    add_column :products, :processed,:boolean
    add_column :products, :error_message,:text
    add_column :products, :user_id,:integer
  end
end
