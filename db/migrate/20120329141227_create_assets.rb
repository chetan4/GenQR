class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :name
      t.string :asset
      t.text :error_encountered
      t.string :status
      t.datetime :uploaded_at
      t.integer :user_id
      t.boolean :processed
      t.timestamps
    end
  end
end
