class CreateValidationErrors < ActiveRecord::Migration
  def change
    create_table :validation_errors do |t|
      t.string :row_id
      t.text :error_encountered

      t.timestamps
    end
  end
end
