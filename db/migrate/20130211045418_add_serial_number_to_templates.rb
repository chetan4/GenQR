class AddSerialNumberToTemplates < ActiveRecord::Migration
  def change
  	add_column :sticker_templates, :serial_number, :boolean
  	add_column :sticker_templates, :serial_number_width, :integer
  	add_column :sticker_templates, :serial_number_height, :integer
  	add_column :sticker_templates, :serial_number_font, :integer
  	add_column :sticker_templates, :serial_number_position, :string
  	add_column :sticker_templates, :serial_number_style, :string
  end
end
