class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :make
      t.string :model
      t.string :serial_number
      t.integer :unit
      t.string :vin
      t.text :notes
      t.timestamps
    end
  end
end
