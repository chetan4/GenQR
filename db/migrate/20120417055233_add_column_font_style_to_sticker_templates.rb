class AddColumnFontStyleToStickerTemplates < ActiveRecord::Migration
  def change
    add_column :sticker_templates, :note_font_style, :string
    add_column :sticker_templates, :default_template ,:boolean
    add_column :sticker_templates, :unit_font_style, :string

  end
end
