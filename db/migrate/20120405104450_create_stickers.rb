class CreateStickers < ActiveRecord::Migration
  def change
    create_table :stickers do |t|
      t.integer :sticker_template_id
      t.integer :asset_id

      t.timestamps
    end
  end
end
