class AddColumnToValidationError < ActiveRecord::Migration
  def change
    add_column :validation_errors, :asset_id, :integer

  end
end
