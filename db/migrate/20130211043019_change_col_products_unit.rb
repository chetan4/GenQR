class ChangeColProductsUnit < ActiveRecord::Migration
  def change
  	change_column :products, :unit, :string
  end
end
