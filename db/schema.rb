# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130211045418) do

  create_table "assets", :force => true do |t|
    t.string   "name"
    t.string   "asset"
    t.text     "error_encountered"
    t.string   "status"
    t.datetime "uploaded_at"
    t.integer  "user_id"
    t.boolean  "processed"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "make"
    t.string   "model"
    t.string   "serial_number"
    t.string   "unit"
    t.string   "vin"
    t.text     "notes"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "asset_id"
    t.text     "qrcode_url"
    t.boolean  "processed"
    t.text     "error_message"
    t.integer  "user_id"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "sticker_templates", :force => true do |t|
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "name"
    t.boolean  "logo"
    t.string   "logo_bg_color"
    t.boolean  "qrcode",                 :default => true
    t.string   "qrcode_bg_color"
    t.boolean  "footer"
    t.string   "footer_bg_color"
    t.text     "body"
    t.integer  "width"
    t.integer  "height"
    t.integer  "logo_width"
    t.integer  "logo_height"
    t.integer  "qrcode_width"
    t.integer  "qrcode_height"
    t.boolean  "notes"
    t.boolean  "unit"
    t.integer  "note_font"
    t.integer  "note_width"
    t.integer  "note_height"
    t.integer  "unit_width"
    t.integer  "unit_height"
    t.integer  "unit_font"
    t.string   "note_position"
    t.string   "unit_position"
    t.string   "logo_image"
    t.integer  "user_id"
    t.string   "note_font_style"
    t.boolean  "default_template"
    t.string   "unit_font_style"
    t.boolean  "serial_number"
    t.integer  "serial_number_width"
    t.integer  "serial_number_height"
    t.integer  "serial_number_font"
    t.string   "serial_number_position"
    t.string   "serial_number_style"
  end

  create_table "stickers", :force => true do |t|
    t.integer  "sticker_template_id"
    t.integer  "asset_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name",              :default => ""
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "admin",                  :default => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "validation_errors", :force => true do |t|
    t.string   "row_id"
    t.text     "error_encountered"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "asset_id"
  end

end
